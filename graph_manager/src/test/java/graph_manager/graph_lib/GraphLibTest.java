package graph_manager.graph_lib;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import graph_manager.graph_lib.GraphLib.Vertex;

public class GraphLibTest {	
	private final Vertex A = Vertex.create("A");
	private final Vertex B = Vertex.create("B");
	private final Vertex C = Vertex.create("C");
	private final Vertex D = Vertex.create("D");
	private final Vertex E = Vertex.create("E");
	
	/**
	 * Default constructor. Needs for JUint test.
	 */
	public GraphLibTest() {
		
	}

	
    @Test (expected = IllegalArgumentException.class)  
    public void testLibAddEdgeParameters() {
		GraphLib lib = new GraphLib(true);

		lib.addEdge(A, B);
	}
    
    @Test (expected = IllegalArgumentException.class)  
    public void testLibAddEdgeParameters1() {
		GraphLib lib = new GraphLib(true);
		
		lib.addVertex(A);
		lib.addEdge(A, B);
	}
    
    @Test (expected = IllegalArgumentException.class)  
    public void testLibAddVertexParameters() {
		GraphLib lib = new GraphLib(true); 

		lib.addVertex(A);
		lib.addVertex(B);
		lib.addVertex(A);
	}
	
    @Test
    public void testLibAsList() {
		GraphLib lib = new GraphLib(true); 
		
		lib.addVertex(A);
		lib.addVertex(B);
		lib.addVertex(C);
		lib.addVertex(D);
		lib.addVertex(E);
		
		lib.addEdge(A, B);
		lib.addEdge(B, C);
		lib.addEdge(C, D);
		lib.addEdge(D, E);
		
		List<Vertex> list = lib.getPath(A, E);

		assertTrue(list.get(0).equals(A));
		assertTrue(list.get(1).equals(B));
		assertTrue(list.get(2).equals(C));
		assertTrue(list.get(3).equals(D));
		assertTrue(list.get(4).equals(E));
		
		list = lib.getPath(B, D);
		assertTrue(list.get(0).equals(B));
		assertTrue(list.get(1).equals(C));
		assertTrue(list.get(2).equals(D));
	}
	
	@Test
	public void testLibAsList2() {
		GraphLib lib = new GraphLib(true); 
		
		lib.addVertex(A);
		lib.addVertex(B);
		
		lib.addEdge(A, B);
		
		List<Vertex> list = lib.getPath(A, B);
		System.out.println(list);
		assertTrue(list.get(0).equals(A));
		assertTrue(list.get(1).equals(B));				
	}
	
	
	@Test (expected = IllegalArgumentException.class)
	public void testLibAsListBadVertex() {
		GraphLib lib = new GraphLib(true); 
		
		lib.addVertex(A);
		lib.addVertex(B);
		
		lib.addEdge(A, E);
	}	
}
