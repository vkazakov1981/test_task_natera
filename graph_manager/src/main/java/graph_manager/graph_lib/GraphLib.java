package graph_manager.graph_lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class GraphLib {
	/**
	 * The map represents edges Vertex to Vertexes. Example the vertex A has edges to {B,C,D.} 
	 */
	private final Map<Vertex, Set<Vertex>> vertices = new HashMap<>();
	private final boolean directedEdges;
	
	public GraphLib(boolean directedEdges) {
		this.directedEdges = directedEdges; 
	}
	
	public void addVertex(Vertex vertex) throws IllegalArgumentException {
		if(vertices.containsKey(vertex)) {
			throw new IllegalArgumentException(String.format("The Graph lib already has vertex with name [%s]", vertex.getName()));
		}
		
		vertices.computeIfAbsent(vertex, e -> new HashSet<>());
	}
	
	public void addEdge(Vertex vertexNameFrom, Vertex vertexNameTo) throws IllegalArgumentException {
		validateAddEdgeParameters(vertexNameFrom, vertexNameTo);
		vertices.get(vertexNameFrom).add(vertexNameTo);
		if(!directedEdges) {
			vertices.get(vertexNameTo).add(vertexNameFrom);
		}
	}
	public void validateAddEdgeParameters(Vertex vertexNameFrom, Vertex vertexNameTo) {
		if(vertexNameFrom.equals(vertexNameTo)) {
			throw new IllegalArgumentException("Can not create edge from Vertex(" + vertexNameFrom + ") to same Vertex" );
		}
		
		if(!vertices.containsKey(vertexNameFrom)) {
			throw new IllegalArgumentException(String.format("The Vertex(From) [%s] is not existing, may be you need add it fierstly", vertexNameFrom));
		}
		
		if(!vertices.containsKey(vertexNameTo)) {
			throw new IllegalArgumentException(String.format("The Vertex (To) [%s] is not existing, may be you need add it fierstly", vertexNameFrom));
		}
	}
	
	public List<Vertex> getPath(Vertex vertexNameFrom, Vertex vertexNameTo) throws IllegalArgumentException {
		
		validateAddEdgeParameters(vertexNameFrom, vertexNameTo);

		Stack<Vertex> path = new Stack<>();
		path.push(vertexNameFrom);					
		boolean found = getPath(vertexNameTo, path);
		if(!found) {
			String message = String.format("The path between %s and %s not found", vertexNameFrom, vertexNameTo);
			throw new IllegalArgumentException(message);
		}

		return new ArrayList<>(path);
	}
	
	private boolean getPath(Vertex targetVertexName, Stack<Vertex> path) {
		Vertex vertex = path.peek();
		
		Set<Vertex> nextVertexes = this.vertices.get(vertex);
		if(nextVertexes.contains(targetVertexName)) {
			path.push(targetVertexName);
			return true;
		}

		for (Vertex nextVertex : nextVertexes) {
			if (!path.contains(nextVertex)) {
				path.push(nextVertex);
				if (getPath(targetVertexName, path)) {
					return true;
				}
				path.pop();
			}
		}

		return false;
	}
	
	public static final class Vertex {		

		private final String name;

		public Vertex (String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Vertex other = (Vertex) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}
		
		@Override
		public String toString() {
			return "Vertex [name=" + name + "]";
		}
		
		public static Vertex create(String name) {
			return new Vertex(name);
		}
	}
}
